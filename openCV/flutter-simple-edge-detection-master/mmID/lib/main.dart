import 'dart:convert';
import 'dart:io';
import 'package:aitl/ui/welcome_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/services.dart';
import 'ui/utils/mixin.dart';
import 'config/MyTheme.dart';
import 'config/Server.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  await PermissionHandler()
      .requestPermissions([PermissionGroup.camera, PermissionGroup.microphone]);

  await SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitUp,
      //DeviceOrientation.portraitUp,
      //DeviceOrientation.landscapeLeft,
      //DeviceOrientation.landscapeRight
    ],
  );

  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new GetMaterialApp(
      debugShowCheckedModeBanner: false,
      enableLog: Server.isTest,
      defaultTransition: Transition.fade,
      title: "MortgageMagic-ID Scanner App",
      theme: MyTheme.themeData,
      home: WelcomePage(),
      builder: EasyLoading.init(),
    );
  }
}
