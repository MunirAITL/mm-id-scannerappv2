import 'dart:io';
import 'dart:typed_data';
import 'package:aitl/config/AppCfg.dart';
import 'package:aitl/openCV/scan.dart';
import 'package:aitl/ui/euidcard/euidcard_front_page.dart';
import 'package:aitl/ui/passport/passport_front_page.dart';
import 'package:aitl/ui/utils/ui_mixin.dart';
import 'package:aitl/ui/widgets/Btn.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:aitl/ui/widgets/dropdown/DropDownPicker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:simple_edge_detection/edge_detection.dart';
import 'utils/mixin.dart';
import 'dl/driving_back_page.dart';
import 'dl/driving_front_page.dart';
import 'euidcard/euidcard_back_page.dart';
import 'list_doc_page.dart';
import 'passport/passport_back_page.dart';
import 'widgets/dropdown/DropListModel.dart';
import 'dart:math' as math;
//import 'package:image/image.dart' as IMG;
import 'package:image/image.dart';
import 'dart:io';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:tesseract_ocr/tesseract_ocr.dart';

class PickScanDocPage extends StatefulWidget {
  const PickScanDocPage({Key key}) : super(key: key);
  @override
  State createState() => _PickScanDocPageState();
}

class _PickScanDocPageState extends State<PickScanDocPage> with Mixin, UIMixin {
  DropListModel caseDD = DropListModel([]);
  OptionItem caseOpt = OptionItem(id: null, title: "Select Documents Type");

  bool isFrontSide = true;
  int width, height;

  Future<void> openCamera({isFront = true, isCam = true}) async {
    PickedFile pickedFile;

    if (!isCam) {
      pickedFile = await ImagePicker().getImage(
        source: ImageSource.gallery,
        imageQuality: 100,
        //maxWidth: 100000.0,
        //maxHeight: 100000.0,
      );
    } else {
      pickedFile = await ImagePicker().getImage(
        source: ImageSource.camera,
        imageQuality: 100,
        //maxWidth: 100000.0,
        //maxHeight: 100000.0,
        preferredCameraDevice:
            (isFront) ? CameraDevice.front : CameraDevice.rear,
      );
    }

    if (pickedFile != null) {
      if (mounted) {
        EasyLoading.show(status: 'Please wait...');
        var file = File(pickedFile.path);
        EdgeDetectionResult edgeDetectionResult =
            await EdgeDetector().detectEdges(file.path);
        bool result = await EdgeDetector()
            .processImage(file.path, edgeDetectionResult, 0);
        EasyLoading.dismiss();
        if (result == false) {
          showAlert("Failed to load image. Please try again");
          return;
        }

        imageCache.clearLiveImages();
        imageCache.clear();
        edgeDetectionResult = null;

        go2(file);
      }
    }
  }

  go2(File file2) {
    switch (caseOpt.id) {
      case 0:
        Get.to(() => (isFrontSide)
            ? DrivingFrontPage(
                file: file2,
                index: caseOpt.id,
                width: width,
                height: height,
              )
            : DrivingBackPage(
                file: file2,
                index: caseOpt.id,
              ));
        break;
      case 1:
        Get.to(() => (isFrontSide)
            ? PassPortFrontPage(
                file: file2,
                index: caseOpt.id,
                width: width,
                height: height,
              )
            : PassPortBackPage(
                file: file2,
                index: caseOpt.id,
                width: width,
                height: height,
              ));
        break;
      case 2:
        Get.to(() => (isFrontSide)
            ? EUIDCardFrontPage(
                file: file2,
                index: caseOpt.id,
                width: width,
                height: height,
              )
            : EUIDCardBackPage(
                file: file2,
                index: caseOpt.id,
                width: width,
                height: height,
              ));
        break;
      default:
    }
  }

  @override
  void initState() {
    super.initState();
    List<OptionItem> item = [];
    for (Map map in AppCfg.listDoc) {
      item.add(OptionItem(id: map['id'], title: map['title']));
    }
    caseDD = DropListModel(item);

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: 'Scan Documents',
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return ListView(
      children: [
        Container(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
              child: DropDownPicker(
                cap: null,
                bgColor: Colors.white,
                txtColor: Colors.black,
                txtSize: 17,
                ddTitleSize: .4,
                ddRadius: 5,
                itemSelected: caseOpt,
                dropListModel: caseDD,
                onOptionSelected: (optionItem) {
                  caseOpt = optionItem;
                  setState(() {});
                },
              ),
            ),
          ),
        ),
        SizedBox(height: 40),
        drawIconView(() {
          if (caseOpt.id == null) {
            showAlert("Please choose any document");
          } else {
            isFrontSide = true;
            //Get.to(() => Scan());
            showCamDialog(
                context, (isCam) => {openCamera(isFront: false, isCam: isCam)});
          }
        }, Icons.camera_front_outlined, "Scan Card Front"),
        SizedBox(height: 40),
        drawIconView(() {
          if (caseOpt.id == null) {
            showAlert("Please choose any document");
          } else {
            isFrontSide = false;
            //Get.to(() => Scan());
            showCamDialog(
                context, (isCam) => {openCamera(isFront: false, isCam: isCam)});
          }
        }, Icons.camera_rear_outlined, "Scan Card Back"),
        SizedBox(height: 40),
        Padding(
          padding: const EdgeInsets.all(20),
          child: Btn(
              txt: "Enter Manually",
              txtColor: Colors.white,
              bgColor: Colors.red,
              width: w(context, null),
              height: h(context, 7),
              callback: () {
                Get.to(() => ListDocPage());
              }),
        )
      ],
    );
  }

  drawIconView(callback, icon, text) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Container(
        child: Column(
          children: [
            Icon(
              icon,
              size: 100,
              color: Colors.black,
            ),
            Text(
              text,
              style: TextStyle(color: Colors.black),
            )
          ],
        ),
      ),
    );
  }
}
