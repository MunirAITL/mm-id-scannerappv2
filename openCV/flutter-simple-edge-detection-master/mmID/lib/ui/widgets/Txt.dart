import 'package:flutter/material.dart';

class Txt extends StatelessWidget {
  final txt;
  Color txtColor;
  double txtSize;
  double txtLineSpace;
  TextAlign txtAlign;
  final isBold;
  final isOverflow;
  final int maxLines;

  Txt({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.txtSize,
    @required this.txtAlign,
    @required this.isBold,
    this.txtLineSpace,
    this.isOverflow = false,
    this.maxLines,
  });

  @override
  Widget build(BuildContext context) {
    return //FittedBox(
        //fit: BoxFit.fitWidth,
        //child:
        Text(
      txt,
      textAlign: txtAlign,
      maxLines: maxLines,
      overflow: (isOverflow) ? TextOverflow.ellipsis : TextOverflow.visible,
      style: TextStyle(
        height: txtLineSpace,
        fontSize: txtSize,
        color: txtColor,
        fontWeight: (isBold) ? FontWeight.bold : FontWeight.normal,
      ),
      //),
    );
  }
}
