import 'package:flutter/material.dart';

drawLine({BuildContext context, double w}) {
  return Padding(
    padding: const EdgeInsets.all(5),
    child: Container(width: w, height: 0.5, color: Colors.black),
  );
}
