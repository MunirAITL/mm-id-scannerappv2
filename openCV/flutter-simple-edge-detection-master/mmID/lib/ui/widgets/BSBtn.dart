import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Txt.dart';

class BSBtn extends StatelessWidget {
  final String txt;
  IconData icon;
  Color bgColor;
  Color txtColor;
  double txtSize;
  double height;
  final double radius;
  final double leadingSpace;
  final bool isShowArrow;
  final Function callback;

  BSBtn({
    Key key,
    @required this.txt,
    this.icon,
    this.bgColor,
    this.txtColor,
    @required this.height,
    this.radius = 10,
    this.isShowArrow = true,
    @required this.callback,
    this.leadingSpace = 0,
    this.txtSize,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Container(
        //width: width,
        height: height,
        //color: MyTheme.brownColor,
        decoration: new BoxDecoration(
          color: this.bgColor,
          borderRadius: new BorderRadius.circular(radius),
        ),
        child: ListTile(
          minLeadingWidth: 0,
          leading: Icon(
            icon,
            color: txtColor,
          ),
          title: Txt(
            txt: txt,
            isOverflow: true,
            txtColor: txtColor,
            txtSize: txtSize,
            txtAlign: TextAlign.center,
            isBold: false,
          ),
        ),
      ),
    );
  }
}
