import 'package:aitl/config/AppCfg.dart';
import 'package:aitl/ui/utils/mixin.dart';
import 'package:aitl/ui/passport/passport_back_page.dart';
import 'package:aitl/ui/passport/passport_front_page.dart';
import 'package:aitl/ui/utils/ui_mixin.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'dl/driving_back_page.dart';
import 'dl/driving_front_page.dart';
import 'euidcard/euidcard_back_page.dart';
import 'euidcard/euidcard_front_page.dart';

class ListDocPage extends StatefulWidget {
  const ListDocPage({Key key}) : super(key: key);
  @override
  _ListDocPageState createState() => _ListDocPageState();
}

class _ListDocPageState extends State<ListDocPage> with Mixin, UIMixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: 'Documents',
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: ListView.builder(
        itemCount: AppCfg.listDoc.length,
        itemBuilder: (context, i) {
          Map map = AppCfg.listDoc[i];
          return GestureDetector(
            onTap: () {
              switch (i) {
                case 0:
                  showDocDialog(
                      context,
                      (isFront) => {
                            Get.to(() => (isFront)
                                ? DrivingFrontPage(
                                    file: null,
                                    index: i,
                                  )
                                : DrivingBackPage(
                                    file: null,
                                    index: i,
                                  ))
                          });
                  break;
                case 1:
                  showDocDialog(
                      context,
                      (isFront) => {
                            Get.to(() => (isFront)
                                ? PassPortFrontPage(
                                    file: null,
                                    index: i,
                                  )
                                : PassPortBackPage(
                                    file: null,
                                    index: i,
                                  ))
                          });
                  break;
                case 2:
                  showDocDialog(
                      context,
                      (isFront) => {
                            Get.to(() => (isFront)
                                ? EUIDCardFrontPage(
                                    file: null,
                                    index: i,
                                  )
                                : EUIDCardBackPage(
                                    file: null,
                                    index: i,
                                  ))
                          });
                  break;
                default:
              }
            },
            child: ListTile(
              leading: Icon(
                Icons.file_copy_outlined,
                color: Colors.black,
              ),
              title: Text(
                map['title'],
                style: TextStyle(color: Colors.black, fontSize: 20),
              ),
            ),
          );
        },
      ),
    );
  }
}
