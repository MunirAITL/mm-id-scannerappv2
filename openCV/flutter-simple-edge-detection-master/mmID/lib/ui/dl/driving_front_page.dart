import 'dart:convert';
import 'dart:io';
import 'package:aitl/config/Define.dart';
import 'package:aitl/controller/rx/gender_controller.dart';
import 'package:aitl/ui/utils/mixin.dart';
import 'package:aitl/ui/anim/ScanAnim.dart';
import 'package:aitl/ui/widgets/InputBox.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:typed_data';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import '../widgets/DatePickerView.dart';
import '../base_card.dart';
import 'dl_mixin.dart';

class DrivingFrontPage extends StatefulWidget {
  final File file;
  final int index;
  final int width, height;
  const DrivingFrontPage(
      {Key key, this.file, @required this.index, this.width, this.height})
      : super(key: key);
  @override
  State createState() => _DrivingFrontPageState();
}

class _DrivingFrontPageState extends BaseCard<DrivingFrontPage>
    with DrivingMixin {
  final givenName = TextEditingController();
  final familyName = TextEditingController();
  var dob = "";

  GenderController cGender = Get.put(GenderController());

  bool isAnim = true;

  @override
  void initState() {
    super.initState();
    try {
      if (widget.file != null) {
        Future.delayed(Duration.zero, () async {
          setState(() {
            isAnim = true;
          });

          //  get by google vision
          doGoogleVisionOCR(widget.file, false).then((body) {
            isAnim = false;
            if (body != null) {
              print(body);
              parseFrontSide(body);
              setState(() {
                isAnim = false;
              });
            }
          });
          print("********************************************");
        });
      } else {
        isAnim = false;
      }
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    cGender.dispose();
    givenName.dispose();
    familyName.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final title = "Driver's Licence - Front";
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: title,
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //(image != null) ? image : SizedBox(),
          SizedBox(height: 10),
          ScanAnim(file: widget.file, isAnim: isAnim, assetsImage: "dl1a.jpg"),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InputBox(
                  ctrl: givenName,
                  lableTxt: "Given Name",
                  kbType: TextInputType.name,
                  len: 50,
                  isPwd: false,
                ),
                InputBox(
                  ctrl: familyName,
                  lableTxt: "Family Name",
                  kbType: TextInputType.name,
                  len: 50,
                  isPwd: false,
                ),
                SizedBox(height: 10),
                drawGender(),
                SizedBox(height: 10),
                DatePickerView(
                  cap: 'Date of birth',
                  dt: dob,
                  initialDate: dateDOBlast,
                  firstDate: dateDOBfirst,
                  lastDate: dateDOBlast,
                  txtColor: Colors.black,
                  callback: (value) {
                    if (mounted) {
                      setState(() {
                        try {
                          dob = DateFormat('dd-MMM-yyyy')
                              .format(value)
                              .toString();
                        } catch (e) {}
                      });
                    }
                  },
                ),
                SizedBox(height: 50),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //  *********************************** parsing start

  parseFrontSide(gbody) {
    //  Try with google vision
    try {
      var gbody2 = gbody.replaceAll(".\\n", ".");
      final gbodyArr = gbody2.split("\\n");
      for (int i = 0; i < gbodyArr.length; i++) {
        final txt = gbodyArr[i];
        final v = txt.replaceAll("\n", "").toLowerCase();
        print(v);

        if (v.startsWith("1 ") || v.startsWith("1.")) {
          givenName.text = trim(v);
        } else if (v.startsWith("2 ") || v.startsWith("2.")) {
          familyName.text = trim(v);
        } else if (v.startsWith("3 ") || v.startsWith("3.")) {
          final str = trim(v);
          try {
            final strArr = str.toString().split(" ");
            final dobArr = strArr[0].toString().split(".");
            String dd = dobArr[0];
            String mm = dobArr[1];
            String yy = dobArr[2];
            dob = trim(dd + '-' + Define.listMMM[int.parse(mm)] + '-' + yy);
          } catch (e) {}
        } else if (dob == '') {
          final strArr = v.split(".");
          if (strArr.length > 1) {
            try {
              final strArr = v.split(" ");
              final dobArr = strArr[0].split(".");
              String dd = dobArr[0];
              String mm = dobArr[1];
              String yy = dobArr[2];
              dob = trim(dd + '-' + Define.listMMM[int.parse(mm)] + '-' + yy);
            } catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  parseBackSide(gbody) {}

  //  *********************************** parsing end
}
