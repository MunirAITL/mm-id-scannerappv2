import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:aitl/config/Define.dart';
import 'package:aitl/controller/rx/gender_controller.dart';
import 'package:aitl/ui/anim/ScanAnim.dart';
import 'package:aitl/ui/euidcard/euid_mixin.dart';
import 'package:aitl/ui/widgets/DatePickerView.dart';
import 'package:aitl/ui/widgets/InputBox.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:aitl/ui/widgets/dropdown/DropDownPicker.dart';
import 'package:aitl/ui/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image/image.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import '../base_card.dart';
//import 'package:mypkg/image_processing/src/image.dart' as Image;

class EUIDCardFrontPage extends StatefulWidget {
  File file;
  final int index;
  final int width, height;
  EUIDCardFrontPage(
      {Key key, this.file, @required this.index, this.width, this.height})
      : super(key: key);
  @override
  State createState() => _EUIDCardFrontPageState();
}

class _EUIDCardFrontPageState extends BaseCard<EUIDCardFrontPage>
    with EUIDMixin {
  final cardNo = TextEditingController();
  final mumerCan = TextEditingController();
  final surName = TextEditingController();
  final givenName = TextEditingController();
  final personalNo = TextEditingController();
  final identityCardNo = TextEditingController();

  DropListModel ddMaritalNationalities;
  OptionItem optNationalities;

  GenderController cGender = Get.put(GenderController());

  var dob = "";
  var dtExpiry = "";

  bool isAnim = true;
  bool isData1 = true;

  @override
  void initState() {
    super.initState();
    try {
      initPage();
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    cGender.dispose();
    cardNo.dispose();
    mumerCan.dispose();
    surName.dispose();
    givenName.dispose();
    personalNo.dispose();
    identityCardNo.dispose();
    super.dispose();
  }

  initPage() async {
    try {
      await getNationaity(context: context)
          .then((DropListModel ddMaritalNationalities2) {
        ddMaritalNationalities = ddMaritalNationalities2;
        optNationalities = OptionItem(id: 0, title: "Nationality");
        startOCR();
      });
    } catch (e) {}
  }

  startOCR() {
    if (widget.file != null) {
      Future.delayed(Duration.zero, () async {
        setState(() {
          isAnim = true;
        });

        //  cropping image for getting card object excluding background for more clear data processing
        //  https://github.com/brendan-duncan/image/blob/master/example/example.dart
        /*final image = decodeImage(widget.file.readAsBytesSync());
          List<int> trimRect = findTrim(image, mode: TrimMode.transparent);
          final image2 = copyCrop(
              image, trimRect[0], trimRect[1], trimRect[2], trimRect[3]);
          widget.file
              .writeAsBytesSync(encodeNamedImage(image2, widget.file.path));*/

        //  get by firebase
        //final FirebaseVisionTextDetector detector =
        //FirebaseVisionTextDetector.instance;
        //final currentLabels = await detector.detectFromPath(widget.file?.path);

        //  get by google vision
        doGoogleVisionOCR(widget.file, false).then((body) {
          isAnim = false;
          if (body != null) {
            print(body);
            parseFrontSide(body);
            setState(() {
              isAnim = false;
            });
          }
        });
        print("********************************************");
      });
    } else {
      isAnim = false;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final title = "EU ID Card - Front";

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: title,
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //(image != null) ? image : SizedBox(),
          SizedBox(height: 10),
          ScanAnim(file: widget.file, isAnim: isAnim, assetsImage: "eu1a.png"),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              InputBox(
                ctrl: cardNo,
                lableTxt: "Card No",
                kbType: TextInputType.name,
                len: 15,
                isPwd: false,
              ),
              InputBox(
                ctrl: identityCardNo,
                lableTxt: "Identity Card No",
                kbType: TextInputType.name,
                len: 15,
                isPwd: false,
              ),
              InputBox(
                ctrl: mumerCan,
                lableTxt: "Mumer Can",
                kbType: TextInputType.name,
                len: 15,
                isPwd: false,
              ),
              InputBox(
                ctrl: surName,
                lableTxt: "SurName",
                kbType: TextInputType.name,
                len: 50,
                isPwd: false,
              ),
              InputBox(
                ctrl: givenName,
                lableTxt: "Given Name",
                kbType: TextInputType.name,
                len: 50,
                isPwd: false,
              ),
              InputBox(
                ctrl: personalNo,
                lableTxt: "Personal No",
                kbType: TextInputType.name,
                len: 20,
                isPwd: false,
              ),
              (optNationalities != null)
                  ? DropDownPicker(
                      cap: "Nationality",
                      itemSelected: optNationalities,
                      dropListModel: ddMaritalNationalities,
                      onOptionSelected: (optionItem) {
                        optNationalities = optionItem;
                        setState(() {});
                      },
                    )
                  : SizedBox(),
              SizedBox(height: 20),
              DatePickerView(
                cap: 'Date of birth',
                dt: dob,
                initialDate: dateDOBlast,
                firstDate: dateDOBfirst,
                lastDate: dateDOBlast,
                txtColor: Colors.black,
                callback: (value) {
                  if (mounted) {
                    setState(() {
                      try {
                        dob =
                            DateFormat('dd-MMM-yyyy').format(value).toString();
                      } catch (e) {}
                    });
                  }
                },
              ),
              SizedBox(height: 10),
              drawGender(),
              SizedBox(height: 10),
              DatePickerView(
                cap: 'Date of expiry',
                dt: dtExpiry,
                initialDate: DateTime.now(),
                firstDate: DateTime.now(),
                lastDate:
                    DateTime(dateNow.year + 50, dateNow.month, dateNow.day),
                txtColor: Colors.black,
                callback: (value) {
                  if (mounted) {
                    setState(() {
                      try {
                        dtExpiry =
                            DateFormat('dd-MMM-yyyy').format(value).toString();
                      } catch (e) {}
                    });
                  }
                },
              ),
              SizedBox(height: 50),
            ]),
          ),
        ],
      ),
    );
  }

  //  *********************************** parsing start

  @override
  parseFrontSide(gbody) {
    try {
      //  Try with google vision
      var gbody2 = gbody.replaceAll(".\\n", ".");
      final gbodyArr = gbody2.split("\\n");

      //var gbody2 = gbody.replaceAll(".\n", ".");
      //final gbodyArr = gbody2.split("\n");

      for (int i = 0; i < gbodyArr.length; i++) {
        final txt = gbodyArr[i];
        final v = txt.replaceAll("\n", "").toLowerCase();
        print(v);

        if (v.toLowerCase().startsWith("latvijas r")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          String mCardNo = getCardNo(v2);
          cardNo.text = mCardNo;
        }
        if (v.toLowerCase().startsWith("mumer can")) {
          String str = v.toLowerCase().replaceAll("mumer can", "").trim();
          if (str.length > 0) {
            mumerCan.text = str;
          } else {
            final txt2 = gbodyArr[i + 1];
            final v2 = txt2.replaceAll("\n", "");
            try {
              if (int.parse(v2) > 0) mumerCan.text = v2;
            } catch (e) {}
          }
        }
        if (v.toLowerCase().startsWith("signature d")) {
          try {
            final strArr = v.split(" ");
            if (int.parse(strArr[strArr.length - 1]) > 0) {
              mumerCan.text = strArr[strArr.length - 1];
            }
          } catch (e) {}
          if (mumerCan.text.isEmpty) {
            final txt2 = gbodyArr[i + 1];
            final v2 = txt2.replaceAll("\n", "");
            try {
              if (int.parse(v2) > 0) {
                mumerCan.text = v2;
              }
            } catch (e) {}
          }
        }
        if (v.toLowerCase().startsWith("nazwisko /") ||
            v.toLowerCase().startsWith("Uzvards/") ||
            v.toLowerCase().contains("surname")) {
          try {
            final txt2 = gbodyArr[i + 1];
            final v2 = txt2.replaceAll("\n", "");
            surName.text = v2;
          } catch (e) {}
        } else if (v.toLowerCase().startsWith("Vardsl") ||
            v.toLowerCase().startsWith("mona /") ||
            v.toLowerCase().contains("given n")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          givenName.text = v2;
        } else if (v.toLowerCase().contains("nationality")) {
          String txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "");
          if (v2.contains("-")) {
            final strArr = v2.split(" ");
            if (strArr.length == 1) {
              personalNo.text = strArr[0];
            } else if (strArr.length > 1) {
              personalNo.text = strArr[0];
              optNationalities.title = strArr[1];
            }
          }

          if (optNationalities.title == "Nationality") {
            bool isString = hasStringOnly(v2);
            if (isString) {
              optNationalities.title = v2;
            }
          }
          if (personalNo.text.isEmpty) {
            txt2 = gbodyArr[i + 2];
            v2 = txt2.replaceAll("\n", "");
            if (v2.contains("-")) {
              personalNo.text = v2;
            }
          }
        }
        if (v.toLowerCase().contains("date of birth")) {
          String txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "");
          try {
            final strArr = v2.split(".");
            if (strArr.length > 2) {
              int pos = int.parse(strArr[1]);
              String mm = Define.listMMM[pos - 1];
              dob = strArr[0] + "-" + mm + "-" + strArr[2];
            }
          } catch (e) {}
          if (dob == "") {
            String txt2 = gbodyArr[i + 2];
            String v2 = txt2.replaceAll("\n", "");
            try {
              final strArr = v2.split(".");
              if (strArr.length > 2) {
                int pos = int.parse(strArr[1]);
                String mm = Define.listMMM[pos - 1];
                dob = strArr[0] + "-" + mm + "-" + strArr[2];
              }
            } catch (e) {}
          }
        }
        if ((v.toLowerCase().contains("termin") ||
                v.toLowerCase().contains("expiry")) &&
            v.toLowerCase().contains("date")) {
          String txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "");
          try {
            final strArr = v2.split(".");
            if (strArr.length > 2) {
              int pos = int.parse(strArr[1]);
              String mm = Define.listMMM[pos - 1];
              dtExpiry = strArr[0] + "-" + mm + "-" + strArr[2];
            }
          } catch (e) {}
        }
        if (v.toLowerCase().contains("date of expiry")) {
          String txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "");
          try {
            final strArr = v2.split(".");
            if (strArr.length > 2) {
              int pos = int.parse(strArr[1]);
              String mm = Define.listMMM[pos - 1];
              dtExpiry = strArr[0] + "-" + mm + "-" + strArr[2];
            }
          } catch (e) {}
        }
        if (v.toLowerCase().endsWith("sex")) {
          String txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "").toLowerCase();
          if (v2.length == 1) {
            if (v2 == 'm') cGender.gender = genderEnum.male.obs;
            if (v2 == 'f') cGender.gender = genderEnum.female.obs;
          }
          txt2 = gbodyArr[i + 2];
          v2 = txt2.replaceAll("\n", "");
          if (v2.length > 0) {
            identityCardNo.text = v2;
          }
        }
      }
    } catch (e) {}
  }

  @override
  parseBackSide(gbody) {}

  //  *********************************** parsing end
}
