import 'dart:io';

import 'package:aitl/ui/utils/mixin.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'ImageScannerAnimation.dart';

class ScanAnim extends StatefulWidget {
  final bool isAnim;
  final File file;
  final String assetsImage;
  const ScanAnim(
      {@required this.file, @required this.isAnim, @required this.assetsImage});
  @override
  State createState() => _ScanAnimState();
}

class _ScanAnimState extends State<ScanAnim>
    with SingleTickerProviderStateMixin, Mixin {
  AnimationController _animationController;
  bool _animationStopped = false;
  bool scanning = false;

  @override
  void initState() {
    if (widget.isAnim) {
      _animationController = new AnimationController(
          duration: new Duration(seconds: 1), vsync: this);
      _animationController.addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          animateScanAnimation(true);
        } else if (status == AnimationStatus.dismissed) {
          animateScanAnimation(false);
        }
      });
      animateScanAnimation(false);
      setState(() {
        _animationStopped = false;
        scanning = true;
      });
    }
    super.initState();
  }

  @override
  void dispose() {
    if (_animationController != null) _animationController.dispose();
    super.dispose();
  }

  void animateScanAnimation(bool reverse) {
    if (reverse) {
      _animationController.reverse(from: 1.0);
    } else {
      _animationController.forward(from: 0.0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: SafeArea(
        child: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      //height: 20,
      width: w(context, null),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Stack(
            children: [
              Container(
                width: w(context, null),
                height: h(context, 30),
                child: Image(
                  //width: w(context: context),
                  fit: BoxFit.fill,
                  image: (widget.file != null)
                      ? FileImage(widget.file)
                      : AssetImage('assets/images/cards/' + widget.assetsImage),
                ),
              ),
              (widget.isAnim)
                  ? ImageScannerAnimation(
                      _animationStopped,
                      w(context, null),
                      animation: _animationController,
                    )
                  : SizedBox(),
            ],
          ),
        ],
      ),
    );
  }
}
