mixin ParserMixin {
  genericParser(
      int i,
      List<String> gbodyArr,
      String str,
      List<dynamic> listArrExists,
      List<dynamic> listArrNOTExists,
      RegExp regx) {
    String data = "";
    for (Map map in listArrExists) {
      if (map['startWith'] as bool) {
        if (str.toLowerCase().startsWith(map['str'].toString().toLowerCase())) {
          data = parse(i, gbodyArr, str, map, listArrNOTExists, regx);
          if (data != null) break;
        }
      } else if (map['contains'] as bool) {
        if (str.toLowerCase().contains(map['str'].toString().toLowerCase())) {
          data = parse(i, gbodyArr, str, map, listArrNOTExists, regx);
          if (data != null) break;
        }
      } else if (map['endWith'] as bool) {
        if (str.toLowerCase().endsWith(map['str'].toString().toLowerCase())) {
          data = parse(i, gbodyArr, str, map, listArrNOTExists, regx);
          if (data != null) break;
        }
      }
    }
    return data;
  }

  parse(int i, List<String> gbodyArr, String str, Map mapExists,
      List<dynamic> listArrNOTExists, RegExp regx) {
    String data;

    String txt = gbodyArr[i + 1];
    String v = txt.replaceAll("\n", "");

    bool isNextLine = false;
    for (Map map in listArrNOTExists) {
      if (map['startWith']) {
        if (v.toLowerCase().startsWith(map['str'])) {
          isNextLine = true;
          break;
        }
      } else if (map['contains']) {
        if (v.toLowerCase().contains(map['str'])) {
          isNextLine = true;
          break;
        }
      } else if (map['endWith']) {
        if (v.toLowerCase().endsWith(map['str'])) {
          isNextLine = true;
          break;
        }
      }
    }

    if (isNextLine) {
      String txt = gbodyArr[i + 2];
      String v = txt.replaceAll("\n", "");
      if (regx != null) {
        if (regx.hasMatch(v)) {
          data = v;
        }
      } else
        data = v;
    } else {
      if (regx != null) {
        if (regx.hasMatch(v)) {
          data = v;
        }
      } else
        data = v;
    }

    return data;
  }

  findCommonCharacters(String str1, String str2) {
    int len = str1.length;
    //Set<String> uniqueList={};
    int i = 0;
    for (int i = 0; i < str1.length; i++) {
      if (str2.contains(str1[i])) {
        //uniqueList.add(str1[i]);
        //print("val : ${str1[i]}");
        i++;
      }
    }
    if (i >= (len - 2))
      return true;
    else
      return false;
  }
}
