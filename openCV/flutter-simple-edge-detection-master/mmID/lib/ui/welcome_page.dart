import 'package:aitl/ui/pick_scan_doc.dart';
import 'package:aitl/ui/widgets/BSBtn.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'utils/mixin.dart';
import 'list_doc_page.dart';

class WelcomePage extends StatefulWidget {
  @override
  State createState() => _WelcomeState();
}

class _WelcomeState extends State<WelcomePage> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  drawHeader() => new RichText(
        text: new TextSpan(
          // Note: Styles for TextSpans must be explicitly defined.
          // Child text spans will inherit styles from parent
          style: new TextStyle(
            fontSize: 20,
            color: Colors.black,
          ),
          children: <TextSpan>[
            new TextSpan(
                text: 'Magic ',
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: 30)),
            new TextSpan(
                text: 'ID',
                style: new TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 30)),
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          //color: Colors.black,
          width: w(context, null),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            //mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 80),
              drawHeader(),
              SizedBox(height: 40),
              Text("Let's Get Started",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 25)),
              SizedBox(height: 5),
              Text("Scan your documents",
                  style: TextStyle(color: Colors.black, fontSize: 17)),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Container(
                  child: BSBtn(
                      icon: Icons.mobile_screen_share_outlined,
                      txt: "Scan Documents",
                      bgColor: Colors.black,
                      txtColor: Colors.white,
                      txtSize: 20,
                      height: 50,
                      callback: () {
                        Get.to(() => PickScanDocPage());
                      }),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Container(
                  child: BSBtn(
                      icon: Icons.link,
                      txt: "View Documents",
                      bgColor: Colors.black,
                      txtColor: Colors.white,
                      txtSize: 20,
                      height: 50,
                      callback: () {
                        Get.to(() => ListDocPage());
                      }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
