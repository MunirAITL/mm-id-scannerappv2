import 'package:aitl/ui/utils/mixin.dart';
import 'package:get/get.dart';

enum genderEnum { male, female }

class GenderController extends GetxController {
  var gender = genderEnum.male.obs;
}
