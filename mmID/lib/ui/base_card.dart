import 'package:aitl/ui/utils/mixin.dart';
import 'package:aitl/ui/utils/fun.dart';
import 'package:aitl/ui/passport/pp_mixin.dart';
import 'package:aitl/ui/utils/parser.dart';
import 'package:aitl/ui/utils/ui_mixin.dart';
import 'package:flutter/material.dart';

abstract class BaseCard<T extends StatefulWidget> extends State<T>
    with Mixin, Fun, UIMixin, ParserMixin {
  parseFrontSide(String gbody);
  parseBackSide(String gbody);
}
