import 'dart:convert';
import 'dart:io';

import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/ui/anim/ScanAnim.dart';
import 'package:aitl/ui/passport/pp_mixin.dart';
import 'package:aitl/ui/widgets/DatePickerView.dart';
import 'package:aitl/ui/widgets/InputBox.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:aitl/ui/widgets/dropdown/DropDownPicker.dart';
import 'package:aitl/ui/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

import '../base_card.dart';

class PassPortBackPage extends StatefulWidget {
  final File file;
  final int index;
  final int width, height;
  const PassPortBackPage(
      {Key key, this.file, @required this.index, this.width, this.height})
      : super(key: key);
  @override
  State createState() => _PassPortBackPageState();
}

enum genderEnum { male, female }

class _PassPortBackPageState extends BaseCard<PassPortBackPage>
    with PassportMixin {
  bool isAnim = true;

  @override
  void initState() {
    super.initState();
    try {
      if (widget.file != null) {
        Future.delayed(Duration.zero, () async {
          setState(() {
            isAnim = true;
          });

          //  get by firebase
          //final FirebaseVisionTextDetector detector =
          //FirebaseVisionTextDetector.instance;
          //final currentLabels =
          //await detector.detectFromPath(widget.file?.path);

          //  get by google vision
          doGoogleVisionOCR(widget.file).then((body) {
            isAnim = false;
            if (body != null) {
              print(body);
              parseFrontSide(body);
              setState(() {
                isAnim = false;
              });
            }
          });
          print("********************************************");
        });
      } else {
        isAnim = false;
      }

      /*if (widget.scanData != null) {
        widget.scanData.forEach((e) {
          if (e.toString().startsWith("1")) {
            givenName.text = trim(e.toString());
          } else if (e.toString().startsWith("2")) {
            familyName.text = trim(e.toString());
          } else if (e.toString().startsWith("3")) {
            dob = trim(e);
            try {
              final dobArr = dob.toString().split(" ");
              DateTime dt = new DateFormat("dd.MM.yyyy").parse(dobArr[0]);
              dob = Jiffy(dt).format("dd-MMM-yyyy");
            } catch (e) {}
          }
          setState(() {});
        });
      }*/
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  //  *********************************** parsing start

  parseFrontSide(String gbody) {}

  parseBackSide(String gbody) {}

  //  *********************************** parsing end

  @override
  Widget build(BuildContext context) {
    final title = "Passport - Back";
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: title,
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //(image != null) ? image : SizedBox(),
          SizedBox(height: 10),
          ScanAnim(file: widget.file, isAnim: isAnim, assetsImage: "pp_id.png"),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 50),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
