import 'dart:convert';
import 'dart:io';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/rx/gender_controller.dart';
import 'package:aitl/ui/anim/ScanAnim.dart';
import 'package:aitl/ui/passport/pp_mixin.dart';
import 'package:aitl/ui/widgets/DatePickerView.dart';
import 'package:aitl/ui/widgets/InputBox.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:aitl/ui/widgets/dropdown/DropDownPicker.dart';
import 'package:aitl/ui/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import '../utils/mixin.dart';
import '../base_card.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';

class PassPortFrontPage extends StatefulWidget {
  final File file;
  final int index;
  final int width, height;
  const PassPortFrontPage(
      {Key key, this.file, @required this.index, this.width, this.height})
      : super(key: key);
  @override
  State createState() => _PassPortFrontPageState();
}

class _PassPortFrontPageState extends BaseCard<PassPortFrontPage>
    with PassportMixin {
  final ppType = TextEditingController();
  final ppCode = TextEditingController();
  final ppNo = TextEditingController();
  final ppSurName = TextEditingController();
  final ppGivenName = TextEditingController();
  final ppPlaceBirth = TextEditingController();
  final ppAuthority = TextEditingController();

  DropListModel ddMaritalNationalities;
  OptionItem optNationalities;

  GenderController cGender = Get.put(GenderController());

  //genderEnum gender = genderEnum.male;

  var dob = "";
  var dtIssue = "";
  var dtExpiry = "";
  String last2lines = "";

  bool isAnim = true;

  @override
  void initState() {
    super.initState();
    try {
      initPage();
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    cGender.dispose();
    ppType.dispose();
    ppCode.dispose();
    ppNo.dispose();
    ppSurName.dispose();
    ppGivenName.dispose();
    ppPlaceBirth.dispose();
    ppAuthority.dispose();
    super.dispose();
  }

  initPage() async {
    try {
      await getNationaity(context: context)
          .then((DropListModel ddMaritalNationalities2) {
        ddMaritalNationalities = ddMaritalNationalities2;
        optNationalities = OptionItem(id: 0, title: "Nationality");
        setState(() {});
      });

      if (widget.file != null) {
        Future.delayed(Duration.zero, () async {
          setState(() {
            isAnim = true;
          });

          //  get by firebase
          //final FirebaseVisionTextDetector detector =
          //FirebaseVisionTextDetector.instance;
          //final currentLabels =
          //await detector.detectFromPath(widget.file?.path);

          //  get by google vision
          doGoogleVisionOCR(widget.file).then((body) {
            isAnim = false;
            if (body != null) {
              print(body);
              parseFrontSide(body);
              setState(() {
                isAnim = false;
              });
            }
          });
          print("********************************************");
        });
      } else {
        isAnim = false;
      }

      /*if (widget.scanData != null) {
        widget.scanData.forEach((e) {
          if (e.toString().startsWith("1")) {
            givenName.text = trim(e.toString());
          } else if (e.toString().startsWith("2")) {
            familyName.text = trim(e.toString());
          } else if (e.toString().startsWith("3")) {
            dob = trim(e);
            try {
              final dobArr = dob.toString().split(" ");
              DateTime dt = new DateFormat("dd.MM.yyyy").parse(dobArr[0]);
              dob = Jiffy(dt).format("dd-MMM-yyyy");
            } catch (e) {}
          }
          setState(() {});
        });
      }*/
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    final title = "Passport - Front";
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: title,
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //(image != null) ? image : SizedBox(),
          SizedBox(height: 10),
          ScanAnim(file: widget.file, isAnim: isAnim, assetsImage: "pp_id.png"),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InputBox(
                  ctrl: ppType,
                  lableTxt: "Passport Type",
                  kbType: TextInputType.name,
                  len: 1,
                  isPwd: false,
                ),
                InputBox(
                  ctrl: ppCode,
                  lableTxt: "Code",
                  kbType: TextInputType.name,
                  len: 3,
                  isPwd: false,
                ),
                InputBox(
                  ctrl: ppNo,
                  lableTxt: "Passport No",
                  kbType: TextInputType.name,
                  len: 50,
                  isPwd: false,
                ),
                InputBox(
                  ctrl: ppSurName,
                  lableTxt: "Sur Name",
                  kbType: TextInputType.name,
                  len: 50,
                  isPwd: false,
                ),
                InputBox(
                  ctrl: ppGivenName,
                  lableTxt: "Given Name",
                  kbType: TextInputType.name,
                  len: 50,
                  isPwd: false,
                ),
                (optNationalities != null)
                    ? DropDownPicker(
                        cap: "Nationality",
                        itemSelected: optNationalities,
                        dropListModel: ddMaritalNationalities,
                        onOptionSelected: (optionItem) {
                          optNationalities = optionItem;
                          setState(() {});
                        },
                      )
                    : SizedBox(),
                SizedBox(height: 20),
                DatePickerView(
                  cap: 'Date of birth',
                  dt: dob,
                  initialDate: dateDOBlast,
                  firstDate: dateDOBfirst,
                  lastDate: dateDOBlast,
                  txtColor: Colors.black,
                  callback: (value) {
                    if (mounted) {
                      setState(() {
                        try {
                          dob = DateFormat('dd-MMM-yyyy')
                              .format(value)
                              .toString();
                        } catch (e) {}
                      });
                    }
                  },
                ),
                InputBox(
                  ctrl: ppPlaceBirth,
                  lableTxt: "Place of Birth",
                  kbType: TextInputType.name,
                  len: 50,
                  isPwd: false,
                ),
                InputBox(
                  ctrl: ppAuthority,
                  lableTxt: "Authority",
                  kbType: TextInputType.name,
                  len: 50,
                  isPwd: false,
                ),
                SizedBox(height: 20),
                DatePickerView(
                  cap: 'Date of issue',
                  dt: dtIssue,
                  initialDate: DateTime.now(),
                  firstDate:
                      DateTime(dateNow.year - 50, dateNow.month, dateNow.day),
                  lastDate: DateTime.now(),
                  txtColor: Colors.black,
                  callback: (value) {
                    if (mounted) {
                      setState(() {
                        try {
                          dtIssue = DateFormat('dd-MMM-yyyy')
                              .format(value)
                              .toString();
                        } catch (e) {}
                      });
                    }
                  },
                ),
                SizedBox(height: 10),
                drawGender(),
                SizedBox(height: 10),
                DatePickerView(
                  cap: 'Date of expiry',
                  dt: dtExpiry,
                  initialDate: DateTime.now(),
                  firstDate: DateTime.now(),
                  lastDate:
                      DateTime(dateNow.year + 50, dateNow.month, dateNow.day),
                  txtColor: Colors.black,
                  callback: (value) {
                    if (mounted) {
                      setState(() {
                        try {
                          dtExpiry = DateFormat('dd-MMM-yyyy')
                              .format(value)
                              .toString();
                        } catch (e) {}
                      });
                    }
                  },
                ),
                SizedBox(height: 20),
                (last2lines.length > 0)
                    ? Card(
                        elevation: 10,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                            child: Txt(
                                txt: last2lines,
                                txtColor: Colors.red,
                                txtSize: 20,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ),
                        ),
                      )
                    : SizedBox(),
                SizedBox(height: 50),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //  *********************************** parsing start

  parseFrontSide(String gbody) {
    try {
      //  Try with google vision
      var gbody2 = gbody.replaceAll(".\\n", ".");
      final gbodyArr = gbody2.split("\\n");
      for (int i = 0; i < gbodyArr.length; i++) {
        final txt = gbodyArr[i];
        final v = txt.replaceAll("\n", "");
        print(v);

        //  Type
        if (v.startsWith("Type")) {
          String type = getType(v);
          if (type.length == 1) {
            ppType.text = type;
          }
        }
        //  Code
        else if (v.startsWith("Code/Code") || v.startsWith("Code Code")) {
          Map map = getCode(v);
          print(map);
          if (map['code'] == null) {
            final txt2 = gbodyArr[i + 1];
            final v2 = txt2.replaceAll("\n", "");
            if (!v2.startsWith("Passport ")) {
              bool isCode = hasStringOnly(v2);
              if (isCode) ppCode.text = v2;
            }
          }
        }
        //  Passport No
        else if (v.startsWith("Passport ") && v.contains(" No")) {
          Map map = getPassportNo(v);
          print(map);
          if (map['code'] != null) {
            ppCode.text = map['code'];
          } else if (map['ppNo'] != null) {
            ppNo.text = map['ppNo'];
          }
          if (map['ppNo'] == null) {
            final txt2 = gbodyArr[i + 1];
            final v2 = txt2.replaceAll("\n", "");
            ppNo.text = v2;
          }
        }
        //  Surname/Nom
        else if (v.startsWith("Surname") && v.endsWith("1)")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          //bool isString = hasStringOnly(v2);
          //if (isString) ppSurName.text = v2;
          ppSurName.text = v2;
        }
        //  Given names/Prénoms
        else if ((v.startsWith("Given") || v.contains("Nom (")) &&
            v.endsWith("2)")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          //bool isString = hasStringOnly(v2);
          //if (isString) ppGivenName.text = v2;
          ppGivenName.text = v2;
        }
        //  Nationality/Nationalité
        else if (v.startsWith("Nationality") && v.endsWith("3)")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          bool isString = hasStringOnly(v2);
          if (isString) {
            optNationalities.title = v2;
          }
        }
        //  Date of birth/Date de naissance
        else if (v.startsWith("Date") && v.endsWith("4)")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          dob = getPPDate(v2);
        }
        //  Sex/Sexe + Place of birth/Lieu de naissance
        else if (v.startsWith("Sex") && v.endsWith("5)")) {
          String sexStr = getSex(v);
          if (sexStr.length == 1) {
            if (sexStr == 'M') cGender.gender = genderEnum.male.obs;
            if (sexStr == 'F') cGender.gender = genderEnum.female.obs;
          }
          //  Place of birth/Lieu de naissance
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          if (v2.startsWith("Place") && v2.endsWith("6)")) {
            String placeBirthStr = getPlaceBirth(v2);
            if (placeBirthStr.length > 1) {
              ppPlaceBirth.text = placeBirthStr;
            } else {
              //  now getting sex and place of birth NOW...
              final txt2 = gbodyArr[i + 2];
              String v2 = txt2.replaceAll("\n", "");
              if (v2.length == 1) {
                if (v2 == 'M') cGender.gender = genderEnum.male.obs;
                if (v2 == 'F') cGender.gender = genderEnum.female.obs;

                //  now getting place of birth on 2nd row NOW...
                final txt2 = gbodyArr[i + 3];
                v2 = txt2.replaceAll("\n", "");
                bool isStr = hasStringOnly(v2);
                if (isStr) {
                  ppPlaceBirth.text = v2;
                }
              } else {
                bool isStr = hasStringOnly(v2);
                if (isStr) {
                  ppPlaceBirth.text = v2;
                }
              }
            }
          }
        } else if (ppPlaceBirth.text.isEmpty &&
            v.startsWith("Place") &&
            v.endsWith("6)")) {
          //  now getting sex and place of birth NOW...
          final txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "");
          bool isStr = hasStringOnly(v2);
          if (isStr) {
            ppPlaceBirth.text = v2;
          }
        }
        //  Date of issue/Date de délivrance + Authority/Autorité
        else if (v.startsWith("Date") && v.endsWith("7)")) {
          String dtIssueStr = getDateIssue(v);
          if (dtIssueStr.length > 0) {
            dtIssue = dtIssueStr;
          }
          //  Authority/Autorité
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          dtIssueStr = getPPDate(v2);
          if (dtIssueStr.length > 0) {
            dtIssue = dtIssueStr;
          } else if (v2.startsWith("Authority") && v2.endsWith("8)")) {
            String authorityStr = getAuthority(v2);
            if (authorityStr.length > 0) {
              ppAuthority.text = authorityStr;
            } else {
              //  now getting issue/Date de délivrance + Authority/Autorité NOW...
              final txt2 = gbodyArr[i + 2];
              String v2 = txt2.replaceAll("\n", "");
              dtIssueStr = getPPDate(v2);
              if (dtIssueStr.length > 0) {
                dtIssue = dtIssueStr;
                //  now getting Authority/Autorité on 2nd row NOW...
                final txt2 = gbodyArr[i + 3];
                v2 = txt2.replaceAll("\n", "");
                ppAuthority.text = v2;
              } else {
                final txt2 = gbodyArr[i + 3];
                v2 = txt2.replaceAll("\n", "");
                bool isStr = hasStringOnly(v2);
                if (isStr) {
                  ppAuthority.text = v2;
                }
              }
            }
          }
        } else if (ppAuthority.text.isEmpty &&
            v.startsWith("Authority") &&
            (v.endsWith("8)") || v.endsWith("S)"))) {
          final txt2 = gbodyArr[i + 1];
          var v2 = txt2.replaceAll("\n", "");
          bool isStr = hasStringOnly(v2);
          if (isStr) {
            ppAuthority.text = v2;
          }
        }
        //  Date of expiry/Date d'expiration
        else if (v.startsWith("Date") && v.endsWith("9)")) {
          String dtExpiryStr = getDateExpiry(v, "9");
          dtExpiryStr = getPPDate(dtExpiryStr);
          if (dtExpiryStr.length > 0) {
            dtExpiry = dtExpiryStr;
          } else {
            final txt2 = gbodyArr[i + 1];
            final v2 = txt2.replaceAll("\n", "");
            String dtExpiryStr = getPPDate(v2);
            if (dtExpiryStr.length > 0) {
              dtExpiry = dtExpiryStr;
            }
          }
        }
        //  Holder's signature/Signature du titulaire
        else if (dtExpiry == '' &&
            v.startsWith("Holder") &&
            v.endsWith("10)")) {
          String dtExpiryStr = getDateExpiry(v, "10");
          dtExpiryStr = getPPDate(dtExpiryStr);
          if (dtExpiryStr.length > 0) {
            dtExpiry = dtExpiryStr;
          } else {
            final txt2 = gbodyArr[i + 1];
            final v2 = txt2.replaceAll("\n", "");
            String dtExpiryStr = getPPDate(v2);
            if (dtExpiryStr.length > 0) {
              dtExpiry = dtExpiryStr;
            }
          }
        }
      }
    } catch (e) {}
  }

  parseBackSide(String gbody) {}

  //  *********************************** parsing end
}
