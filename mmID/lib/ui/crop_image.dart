import 'dart:io';
import 'dart:typed_data';

import 'package:aitl/config/ImageCfg.dart';
import 'package:aitl/ui/utils/mixin.dart';
import 'package:crop_your_image/crop_your_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';

import 'dl/driving_back_page.dart';
import 'dl/driving_front_page.dart';
import 'euidcard/euidcard_back_page.dart';
import 'euidcard/euidcard_front_page.dart';
import 'passport/passport_back_page.dart';
import 'passport/passport_front_page.dart';
import 'widgets/Txt.dart';

class CropImagePage extends StatefulWidget {
  File file;
  final int index;
  final bool isFrontSide;
  CropImagePage({
    Key key,
    @required this.file,
    @required this.index,
    @required this.isFrontSide,
  }) : super(key: key);
  @override
  State createState() => _CropImagePageState();
}

class _CropImagePageState extends State<CropImagePage> with Mixin {
  final _controller = CropController();

  Uint8List list8Data;
  int width, height;

  @override
  void initState() {
    super.initState();
    //  getting image data for cropping
    widget.file.readAsBytes().then((data) {
      list8Data = data;
      setState(() {});
    });
  }

  @override
  void dispose() {
    list8Data = null;
    super.dispose();
  }

  go2(File file2) {
    EasyLoading.dismiss();
    switch (widget.index) {
      case 0:
        Get.off(() => (widget.isFrontSide)
            ? DrivingFrontPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              )
            : DrivingBackPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              ));
        break;
      case 1:
        Get.off(() => (widget.isFrontSide)
            ? PassPortFrontPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              )
            : PassPortBackPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              ));
        break;
      case 2:
        Get.off(() => (widget.isFrontSide)
            ? EUIDCardFrontPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              )
            : EUIDCardBackPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              ));
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    var title = "Document";
    if (widget.index == 0) {
      if (widget.isFrontSide) {
        title = "Driving Licence - Front";
      } else {
        title = "Driving Licence - Back";
      }
    } else if (widget.index == 1) {
      if (widget.isFrontSide) {
        title = "Passport - Front";
      } else {
        title = "Passport - Back";
      }
    } else if (widget.index == 2) {
      if (widget.isFrontSide) {
        title = "EU ID Card - Front";
      } else {
        title = "EU ID Card - Back";
      }
    }
    if (list8Data != null) {
      EasyLoading.dismiss();
    }

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: title,
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: (list8Data != null) ? drawLayout() : SizedBox(),
      ),
    );
  }

  drawLayout() {
    return Stack(
      alignment: Alignment.center,
      children: [
        Crop(
            image: list8Data,
            cornerDotBuilder: (size, cornerIndex) =>
                const DotControl(color: Colors.black),
            baseColor: Colors.blue.shade900,
            maskColor: Colors.white.withAlpha(100),
            controller: _controller,
            onCropped: (_croppedData) async {
              // do something with image data
              //MemoryImage(_croppedData);
              if (_croppedData != null) {
                //final dir = await getExternalStorageDirectory();
                //final myImagePath = dir.path + "/myimg.png";
                //var f = File(myImagePath);
                //if (await f.exists()) {
                //await f.delete();
                //}
                /*_croppedData = resizeImage(
                  data: _croppedData,
                  width: ImageCfg.width,
                  height: ImageCfg.height,
                );*/
                //var decodedImage = await decodeImageFromList(_croppedData);
                //width = decodedImage.width;
                //height = decodedImage.height;
                go2(await widget.file.writeAsBytes(_croppedData));
              }
            }),
        Positioned(
          top: 0,
          child: Container(
            width: w(context, null),
            color: Colors.black,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Text(
                    'Crop your image',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    'As same as card size',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 16,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(20),
            ),
            child: MaterialButton(
                onPressed: () {
                  EasyLoading.show(status: 'Cropping in progress...');
                  _controller.crop();
                },
                child: Text(
                  'Continue',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                )),
          ),
        ),
      ],
    );
  }
}
