mixin Fun {
  log(str) {
    if (const String.fromEnvironment('DEBUG') != null) {
      //print(str);
      final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
      pattern.allMatches(str).forEach((match) => print(match.group(0)));
    }
  }

  trim(String str) {
    //for (var skip in listSkip) {
    if (str[1] == ".")
      str = str.replaceRange(0, 2, "").trim();
    else
      str = str.replaceRange(0, 1, "").trim();
    //str = str.replaceFirst(skip, "").trim();
    //}
    return str;
  }

  hasStringOnly(String str) {
    return RegExp('[a-zA-Z]').hasMatch(str);
  }

  removeSpecialChar(String str) {
    return str.replaceAll(new RegExp(r'[^\w\s]+'), '');
  }

  changeDate2(String str) {
    try {
      return str[0] + str[1] + '.' + str[3] + str[4] + '.' + str[6] + str[7];
    } catch (e) {
      return str;
    }
  }
}
