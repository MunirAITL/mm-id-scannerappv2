import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/rx/gender_controller.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:aitl/ui/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:path_provider/path_provider.dart';
import 'package:get/get.dart';

import '../widgets/AlrtDialog.dart';

mixin UIMixin {
  Future<DropListModel> getNationaity(
      {BuildContext context, String cap}) async {
    List<OptionItem> list = [];
    String data = await DefaultAssetBundle.of(context)
        .loadString("assets/json/countries.json");
    final jsonResult = json.decode(data);
    int i = 1;
    jsonResult.forEach((element) {
      final optItem = OptionItem(id: i++, title: element['name'].toString());
      list.add(optItem);
    });

    return DropListModel(list);
  }

  drawGender() {
    GenderController cGender = Get.put(GenderController());

    return Obx(
      () => Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Container(
          //color: Colors.yellow,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Txt(
                txt: "Gender",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              Theme(
                data: MyTheme.radioThemeData,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        cGender.gender.value = genderEnum.male;
                      },
                      child: Radio(
                        value: genderEnum.male,
                        groupValue: cGender.gender.value,
                        onChanged: (genderEnum value) {
                          cGender.gender.value = value;
                        },
                      ),
                    ),
                    Txt(
                      txt: "Male",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    ),
                    InkWell(
                      onTap: () {
                        cGender.gender.value = genderEnum.female;
                      },
                      child: Radio(
                        value: genderEnum.female,
                        groupValue: cGender.gender.value,
                        onChanged: (genderEnum value) {
                          cGender.gender.value = value;
                        },
                      ),
                    ),
                    Txt(
                      txt: "Female",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  showAlert(msg) {
    Get.dialog(AlrtDialog(
      msg: msg,
      txtColor: Colors.white,
      bgColor: Colors.red,
    ));
  }

  void showCamDialog(context, bool isNfc, Function(int) callback) {
    showDialog<int>(
      context: context,
      builder: (context) =>
          AlertDialog(content: Text("Choose scanning source"), actions: [
        MaterialButton(
          child: Text("Camera"),
          onPressed: () => Navigator.pop(context, 1),
        ),
        MaterialButton(
          child: Text("Gallery"),
          onPressed: () => Navigator.pop(context, 2),
        ),
        SizedBox(width: 20),
        (isNfc)
            ? MaterialButton(
                child: Text("NFC"),
                onPressed: () => Navigator.pop(context, 0),
              )
            : SizedBox(),
      ]),
    ).then((int isCam) async {
      if (isCam != null) {
        callback(isCam);
      }
    });
  }

  void showDocDialog(context, Function(bool) callback) {
    showDialog<bool>(
      context: context,
      builder: (context) =>
          AlertDialog(content: Text("Choose document side type"), actions: [
        MaterialButton(
          child: Text("Back side"),
          onPressed: () => Navigator.pop(context, false),
        ),
        MaterialButton(
          child: Text("Front side"),
          onPressed: () => Navigator.pop(context, true),
        ),
      ]),
    ).then((bool isFront) async {
      if (isFront != null) {
        callback(isFront);
      }
    });
  }
}
