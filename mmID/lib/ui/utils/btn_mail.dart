import 'package:aitl/ui/utils/mixin.dart';
import 'package:aitl/ui/utils/ui_mixin.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mailer/flutter_mailer.dart';

class MailerBtn extends StatelessWidget with Mixin, UIMixin {
  final String text;
  final pageNo;
  const MailerBtn({Key key, @required this.text, @required this.pageNo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        color: Colors.black,
        child: MaterialButton(
            child: Text(
              "Send NFC Data",
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
            onPressed: () async {
              if (text.length > 0) {
                MailOptions mailOptions = MailOptions(
                  body: text,
                  subject: 'nfcReader-' + pageNo.toString(),
                  recipients: ['munir@aitl.net'],
                  isHTML: true,
                  //bccRecipients: ['other@example.com'],
                  //ccRecipients: ['third@example.com'],
                  //attachments: [ 'path/to/image.png', ],
                );

                final MailerResponse response =
                    await FlutterMailer.send(mailOptions);
                switch (response) {
                  case MailerResponse.saved:

                    /// ios only
                    showAlert('mail was saved to draft');
                    break;
                  case MailerResponse.sent:

                    /// ios only
                    showAlert('mail was sent');
                    break;
                  case MailerResponse.cancelled:

                    /// ios only
                    showAlert('mail was cancelled');
                    break;
                  case MailerResponse.android:
                    showAlert('mail sent successful');
                    break;
                  default:
                    showAlert('email unknown error');
                    break;
                }
              } else {
                showAlert("nfc data not found before sending mail");
              }
            }),
      ),
    );
  }
}
