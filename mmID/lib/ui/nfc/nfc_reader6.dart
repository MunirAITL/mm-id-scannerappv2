import 'dart:async';

import 'package:aitl/ui/utils/btn_mail.dart';
import 'package:aitl/ui/utils/ui_mixin.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';
import 'package:nfc_in_flutter/nfc_in_flutter.dart';
import 'package:nfc_io/nfc_io.dart';
import 'package:nfc_io/nfc_io.dart' as NFCIO;
import 'dart:async';

class NFCReader6 extends StatefulWidget {
  const NFCReader6({Key key}) : super(key: key);
  @override
  _NFCReader6State createState() => _NFCReader6State();
}

class _NFCReader6State extends State<NFCReader6> with UIMixin {
  String txt = "";
  final input = TextEditingController();

  bool isReading = false;
  NFCIO.NfcData data;
  StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    // Check if the device supports NFC reading
    NFC.isNDEFSupported.then((bool isSupported) {
      if (!isSupported) {
        showAlert("this device does not support NFC");
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: 'NFC Test-6',
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Status: ${data?.status ?? "Not ready yet"}',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
                Text(
                  'Card ID: ${data?.id ?? "Unavailable"}',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
                Text(
                  'Card Content: ${data?.content ?? "Unavailable"}',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: input,
              onChanged: (v) {
                txt = v;
                setState(() {});
              },
              showCursor: true,
              readOnly: true,
              style: TextStyle(color: Colors.black, fontSize: 20),
              minLines:
                  20, // any number you need (It works as the rows for the textarea)
              keyboardType: TextInputType.multiline,
              maxLines: null,
              decoration: InputDecoration(
                labelText: "",
                labelStyle: TextStyle(color: Colors.black, fontSize: 20),
                fillColor: Colors.white,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: Colors.blue,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: Colors.red,
                    width: 2.0,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
            child: Container(
              color: Colors.blueAccent,
              child: RawMaterialButton(
                child: Row(
                  children: <Widget>[
                    Icon(
                      isReading ? Icons.stop : Icons.play_arrow,
                      color: Colors.white,
                    ),
                    SizedBox(width: 16),
                    Text(
                      isReading ? "Stop Scanning" : "Start Scanning",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
                onPressed: () async {
                  if (!isReading) {
                    _subscription = NfcIo.startReading.listen((data) {
                      this.data = data;
                      txt += data.content;
                      input.text = txt;
                    });
                  } else {
                    if (_subscription != null) {
                      _subscription.cancel();
                      var result = await NfcIo.stopReading;
                      this.data = result;
                    }
                    txt = "";
                  }
                  setState(() {
                    isReading = !isReading;
                  });
                },
              ),
            ),
          ),
          MailerBtn(text: txt, pageNo: 6),
        ],
      ),
    );
  }
}
