import 'package:aitl/ui/utils/btn_mail.dart';
import 'package:aitl/ui/utils/ui_mixin.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';
import 'package:nfc_in_flutter/nfc_in_flutter.dart';

class NFCReader5 extends StatefulWidget {
  const NFCReader5({Key key}) : super(key: key);
  @override
  _NFCReader5State createState() => _NFCReader5State();
}

class _NFCReader5State extends State<NFCReader5> with UIMixin {
  String txt = "";
  final input = TextEditingController();
  bool _reading = false;
  @override
  void initState() {
    super.initState();
    // Check if the device supports NFC reading
    NFC.isNDEFSupported.then((bool isSupported) {
      if (!isSupported) {
        showAlert("this device does not support NFC");
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: 'NFC Test-5',
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              showCursor: true,
              readOnly: true,
              controller: input,
              onChanged: (v) {
                txt = v;
                setState(() {});
              },
              style: TextStyle(color: Colors.black, fontSize: 20),
              minLines:
                  20, // any number you need (It works as the rows for the textarea)
              keyboardType: TextInputType.multiline,
              maxLines: null,
              decoration: InputDecoration(
                labelText: "",
                labelStyle: TextStyle(color: Colors.black, fontSize: 20),
                fillColor: Colors.white,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: Colors.blue,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: Colors.red,
                    width: 2.0,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
            child: Container(
              color: Colors.blueAccent,
              child: RawMaterialButton(
                child: Text(
                  _reading ? "Stop Scanning" : "Start Scanning",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                onPressed: () async {
                  if (_reading) {
                    FlutterNfcReader.stop();
                    txt = "";
                    setState(() {
                      _reading = false;
                    });
                  } else {
                    try {
                      print('NFC: Scan readed NFC tag');
                      Stream<NfcData> res =
                          FlutterNfcReader.read as Stream<NfcData>;
                      if (res != null) {
                        NfcData response = await res.first;
                        txt += response.content;
                        input.text = txt;
                      }
                    } on PlatformException {
                      print('NFC: Scan stopped exception');
                      showAlert("error on test 5");
                    }
                    setState(() {
                      _reading = true;
                    });
                  }
                },
              ),
            ),
          ),
          MailerBtn(text: txt, pageNo: 5),
        ],
      ),
    );
  }
}
