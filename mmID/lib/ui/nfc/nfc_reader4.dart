import 'dart:convert';

import 'package:aitl/ui/utils/btn_mail.dart';
import 'package:aitl/ui/utils/ui_mixin.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:nfc_in_flutter/nfc_in_flutter.dart';
import 'package:nfc_manager/nfc_manager.dart';

class NFCReader4 extends StatefulWidget {
  const NFCReader4({Key key}) : super(key: key);
  @override
  _NFCReader4State createState() => _NFCReader4State();
}

class _NFCReader4State extends State<NFCReader4> with UIMixin {
  String txt = "";
  final input = TextEditingController();

  ValueNotifier<dynamic> result = ValueNotifier(null);
  bool isAvailable = false;
  bool _reading = false;

  @override
  void initState() {
    super.initState();
    // Check if the device supports NFC reading
    NFC.isNDEFSupported.then((bool isSupported) {
      if (!isSupported) {
        showAlert("this device does not support NFC");
      }
    });
  }

  @override
  void dispose() {
    if (isAvailable) {
      NfcManager.instance.stopSession();
    }
    super.dispose();
  }

  initPage() async {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: 'NFC Test-4',
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: input,
              showCursor: true,
              readOnly: true,
              onChanged: (v) {
                txt = v;
                setState(() {});
              },
              style: TextStyle(color: Colors.black, fontSize: 20),
              minLines:
                  20, // any number you need (It works as the rows for the textarea)
              keyboardType: TextInputType.multiline,
              maxLines: null,
              decoration: InputDecoration(
                labelStyle: TextStyle(color: Colors.black, fontSize: 20),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: Colors.blue,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: Colors.red,
                    width: 2.0,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Container(
              child: RaisedButton(
                  child: Text(
                    _reading ? "Stop reading" : "Start reading",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  onPressed: () {
                    if (_reading) {
                      NfcManager.instance.stopSession();
                      setState(() {
                        _reading = false;
                      });
                    } else {
                      setState(() {
                        _reading = true;
                        NfcManager.instance.startSession(
                          onDiscovered: (NfcTag tag) async {
                            // Do something with an NfcTag instance.
                            txt = json.encode(tag.data);
                            input.text = txt;
                            setState(() {});
                          },
                        );
                      });
                    }
                  }),
            ),
          ),
          MailerBtn(text: txt, pageNo: 4),
        ],
      ),
    );
  }
}
