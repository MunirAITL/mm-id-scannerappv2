import 'dart:async';

import 'package:aitl/ui/utils/btn_mail.dart';
import 'package:aitl/ui/utils/ui_mixin.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nfc_in_flutter/nfc_in_flutter.dart';

class NFCReader3 extends StatefulWidget {
  const NFCReader3({Key key}) : super(key: key);
  @override
  _NFCReader3State createState() => _NFCReader3State();
}

class _NFCReader3State extends State<NFCReader3> with UIMixin {
  String txt = "";
  final input = TextEditingController();

  bool _reading = false;
  StreamSubscription<NDEFMessage> _stream;

  @override
  void initState() {
    super.initState();
    // Check if the device supports NFC reading
    NFC.isNDEFSupported.then((bool isSupported) {
      if (!isSupported) {
        showAlert("this device does not support NFC");
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: 'NFC Test-3',
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return ListView(
      shrinkWrap: true,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            controller: input,
            showCursor: true,
            readOnly: true,
            onChanged: (v) {
              txt = v;
              setState(() {});
            },
            style: TextStyle(color: Colors.black, fontSize: 20),
            minLines:
                20, // any number you need (It works as the rows for the textarea)
            keyboardType: TextInputType.multiline,
            maxLines: null,
            decoration: InputDecoration(
              labelText: "",
              labelStyle: TextStyle(color: Colors.black, fontSize: 20),
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blue,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.red,
                  width: 2.0,
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Container(
            child: RaisedButton(
                child: Text(
                  _reading ? "Stop reading" : "Start reading",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                onPressed: () {
                  if (_reading) {
                    txt = "";
                    _stream?.cancel();
                    setState(() {
                      _reading = false;
                    });
                  } else {
                    setState(() {
                      _reading = true;
                      _stream = NFC
                          .readNDEF(
                        once: true,
                        throwOnUserCancel: false,
                      )
                          .listen((NDEFMessage message) {
                        print("read NDEF message: ${message.payload}");
                        txt += message.payload;
                        input.text = txt;
                      }, onError: (e) {
                        // Check error handling guide below
                        showAlert(e.toString());
                      });
                    });
                  }
                }),
          ),
        ),
        MailerBtn(text: txt, pageNo: 3),
      ],
    );
  }
}
