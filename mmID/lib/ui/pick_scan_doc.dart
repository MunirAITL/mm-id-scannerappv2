import 'dart:io';
import 'dart:typed_data';
import 'package:aitl/config/AppCfg.dart';
import 'package:aitl/ui/crop_image.dart';
import 'package:aitl/ui/euidcard/euidcard_front_page.dart';
import 'package:aitl/ui/nfc_reader_opt.dart';
import 'package:aitl/ui/passport/passport_front_page.dart';
import 'package:aitl/ui/utils/ui_mixin.dart';
import 'package:aitl/ui/widgets/Btn.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:aitl/ui/widgets/dropdown/DropDownPicker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';
import 'package:get/get.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'utils/mixin.dart';
import 'dl/driving_back_page.dart';
import 'dl/driving_front_page.dart';
import 'euidcard/euidcard_back_page.dart';
import 'list_doc_page.dart';
import 'passport/passport_back_page.dart';
import 'widgets/dropdown/DropListModel.dart';
import 'dart:math' as math;
//import 'package:image/image.dart' as IMG;
import 'package:image/image.dart';
import 'dart:io';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:nfc_in_flutter/nfc_in_flutter.dart';

class PickScanDocPage extends StatefulWidget {
  const PickScanDocPage({Key key}) : super(key: key);
  @override
  State createState() => _PickScanDocPageState();
}

class _PickScanDocPageState extends State<PickScanDocPage> with Mixin, UIMixin {
  DropListModel caseDD = DropListModel([]);
  OptionItem caseOpt = OptionItem(id: null, title: "Select Documents Type");

  bool isFrontSide = true;

  Future<void> openCamera({bool isFront = true, int isCam}) async {
    PickedFile pickedFile;
    //EasyLoading.show(status: 'Please wait...');
    if (isCam == 0) {
      Get.to(() => NFCReaderOptPage());
    } else if (isCam == 2) {
      //  Gallery
      pickedFile = await ImagePicker().getImage(
        source: ImageSource.gallery,
        imageQuality: 100,
        //maxWidth: 100000.0,
        //maxHeight: 100000.0,
      );
    } else if (isCam == 1) {
      //  Cam
      pickedFile = await ImagePicker().getImage(
        source: ImageSource.camera,
        imageQuality: 100,
        //maxWidth: 100000.0,
        //maxHeight: 100000.0,
        preferredCameraDevice:
            (isFront) ? CameraDevice.front : CameraDevice.rear,
      );
    }

    if (pickedFile != null) {
      var file = File(pickedFile.path);
      //Image img2 = Image.file(file);
      /*final Uint8List byteData = await file.readAsBytes();
      final decodedImage = decodeImage(byteData);
      //final thumbnail = copyResize(decodedImage, width: 120);
      final image = copyCrop(decodedImage, decodedImage.xOffset,
          decodedImage.yOffset, decodedImage.width, decodedImage.height);

      file.writeAsBytes(encodePng(image));
      print(file.path);
*/
      await Get.to(() => CropImagePage(
            file: file,
            index: caseOpt.id,
            isFrontSide: isFrontSide,
          ));
    }
    //EasyLoading.dismiss();
  }

  @override
  void initState() {
    super.initState();
    List<OptionItem> item = [];
    for (Map map in AppCfg.listDoc) {
      item.add(OptionItem(id: map['id'], title: map['title']));
    }
    caseDD = DropListModel(item);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: 'Scan Documents',
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return ListView(
      children: [
        Container(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
              child: DropDownPicker(
                cap: null,
                bgColor: Colors.white,
                txtColor: Colors.black,
                txtSize: 17,
                ddTitleSize: .4,
                ddRadius: 5,
                itemSelected: caseOpt,
                dropListModel: caseDD,
                onOptionSelected: (optionItem) {
                  caseOpt = optionItem;
                  setState(() {});
                },
              ),
            ),
          ),
        ),
        SizedBox(height: 40),
        drawIconView(() {
          if (caseOpt.id == null) {
            showAlert("Please choose any document");
          } else {
            isFrontSide = true;
            bool isNfc = false;
            if (caseOpt.id == 1) {
              isNfc = true;
            }
            showCamDialog(context, isNfc,
                (isCam) => {openCamera(isFront: false, isCam: isCam)});
          }
        }, Icons.camera_front_outlined, "Scan Card Front"),
        SizedBox(height: 40),
        (caseOpt.id == 0 || caseOpt.id == 1)
            ? SizedBox()
            : drawIconView(() {
                if (caseOpt.id == null) {
                  showAlert("Please choose any document");
                } else {
                  isFrontSide = false;
                  showCamDialog(context, false,
                      (isCam) => {openCamera(isFront: false, isCam: isCam)});
                }
              }, Icons.camera_rear_outlined, "Scan Card Back"),
        SizedBox(height: 40),
        Padding(
          padding: const EdgeInsets.all(20),
          child: Btn(
              txt: "Enter Manually",
              txtColor: Colors.white,
              bgColor: Colors.red,
              width: w(context, null),
              height: h(context, 7),
              callback: () {
                Get.to(() => ListDocPage());
              }),
        )
      ],
    );
  }

  drawIconView(callback, icon, text) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Container(
        child: Column(
          children: [
            Icon(
              icon,
              size: 100,
              color: Colors.black,
            ),
            Text(
              text,
              style: TextStyle(color: Colors.black),
            )
          ],
        ),
      ),
    );
  }
}
