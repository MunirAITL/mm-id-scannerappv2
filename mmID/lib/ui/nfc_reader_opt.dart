import 'dart:async';

import 'package:aitl/ui/nfc/nfc_reader1.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'nfc/nfc_reader2.dart';
import 'nfc/nfc_reader3.dart';
import 'nfc/nfc_reader4.dart';
import 'nfc/nfc_reader5.dart';
import 'nfc/nfc_reader6.dart';

class NFCReaderOptPage extends StatefulWidget {
  const NFCReaderOptPage({Key key}) : super(key: key);

  @override
  _NFCReaderOptPageState createState() => _NFCReaderOptPageState();
}

class _NFCReaderOptPageState extends State<NFCReaderOptPage> {
  final list = [
    "NFC Test 1",
    "NFC Test 2",
    "NFC Test 3",
    "NFC Test 4",
    "NFC Test 5",
    "NFC Test 6",
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: 'NFC Test Selection',
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        children: [
          for (int i = 0; i < list.length; i++)
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
              child: Container(
                color: Colors.black,
                child: MaterialButton(
                    onPressed: () {
                      switch (i) {
                        case 0:
                          Get.to(() => NFCReader1());
                          break;
                        case 1:
                          Get.to(() => NFCReader2());
                          break;
                        case 2:
                          Get.to(() => NFCReader3());
                          break;
                        case 3:
                          Get.to(() => NFCReader4());
                          break;
                        case 4:
                          Get.to(() => NFCReader5());
                          break;
                        case 5:
                          Get.to(() => NFCReader6());
                          break;
                        default:
                      }
                    },
                    child: Text(
                      list[i],
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    )),
              ),
            )
        ],
      ),
    );
  }
}
