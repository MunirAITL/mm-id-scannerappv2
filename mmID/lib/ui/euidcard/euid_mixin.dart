import 'package:aitl/config/Define.dart';
import 'package:intl/intl.dart';

mixin EUIDMixin {
  getCardNo(String str) {
    if (str.length > 0) {
      final strArr = str.split(" ");
      if (strArr.length == 1) {
        return str;
      }
    }
    return "";
  }

  //  29 MAR /MARS 84
  getPPDate(String str) {
    try {
      final strArr = str.split("/");
      final yyArr = strArr[1].split(" ");
      String yyyy = yyArr[1];
      String dtOriginal = strArr[0].replaceAll(" ", "-") + yyyy;

      final dtArr = dtOriginal.split("-");
      String mm = dtArr[1];
      try {
        int i = 1;
        for (var mmm in Define.listMMM) {
          if (mm.toLowerCase().startsWith(mmm.toLowerCase())) {
            break;
          }
          i++;
        }
        final dt2 = dtArr[0] + "-" + i.toString() + "-" + yyyy;
        final twoDigitYear = DateFormat("dd-MM-yy").parse(dt2).year;
        return strArr[0].replaceAll(" ", "-") + twoDigitYear.toString();
      } catch (e) {
        return "";
      }
    } catch (e) {
      return "";
    }
  }
}
