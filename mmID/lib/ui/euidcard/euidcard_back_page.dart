import 'dart:convert';
import 'dart:io';
import 'package:aitl/config/Define.dart';
import 'package:aitl/controller/rx/gender_controller.dart';
import 'package:aitl/ui/anim/ScanAnim.dart';
import 'package:aitl/ui/euidcard/euid_mixin.dart';
import 'package:aitl/ui/widgets/DatePickerView.dart';
import 'package:aitl/ui/widgets/InputBox.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../base_card.dart';

class EUIDCardBackPage extends StatefulWidget {
  final File file;
  final int index;
  final int width, height;
  const EUIDCardBackPage(
      {Key key, this.file, @required this.index, this.width, this.height})
      : super(key: key);
  @override
  State createState() => _EUIDCardBackPageState();
}

class _EUIDCardBackPageState extends BaseCard<EUIDCardBackPage> with EUIDMixin {
  final givenName = TextEditingController();
  final familyName = TextEditingController();
  final placeBirth = TextEditingController();
  final authority = TextEditingController();
  final personalNo = TextEditingController();
  final identityCardNo = TextEditingController();
  final height = TextEditingController();
  final personName = TextEditingController();

  GenderController cGender = Get.put(GenderController());

  var dtIssue = "";

  bool isAnim = true;

  @override
  void initState() {
    super.initState();
    try {
      if (widget.file != null) {
        Future.delayed(Duration.zero, () async {
          setState(() {
            isAnim = true;
          });

          //  get by firebase
          //final FirebaseVisionTextDetector detector =
          //FirebaseVisionTextDetector.instance;
          //final currentLabels =
          //await detector.detectFromPath(widget.file?.path);

          //  get by google vision
          doGoogleVisionOCR(widget.file).then((body) {
            isAnim = false;
            if (body != null) {
              print(body);
              parseFrontSide(body);
              setState(() {
                isAnim = false;
              });
            }
          });

          //parseBackSide([],
          //"NUMER DOWOOU OSOBISTEGO/\nDENTITY CARD NUMER\nNUMER PESEL / PERSONAL NUMBER\n83020410655\nARG 691869\nMIEISCE LRODZENIAI RACE OF BRTH\nWARSZAWA\nNAZWSKO RODOWE IFAMILY NAME\nKOWALSKI\nIMONARODcowIAENTS GIVEN NAMES\nSTEFAN KRYSTYNA\nORGAN WYDAJACY uNG AUTHORITY\nPREZYDENT MIASTA GORZOWA WIELKOPOLSKIEGO\nDATA WYDANIA\nDATE OF ISSUE\n19.05.2029\nI\u003cPOLARG6918690\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\n8202040M2905198POL\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c2\nKOWALSKI\u003cBRZECZYSZCZYKIEWICZ\u003c\u003c\n");
          print("********************************************");
        });
      } else {
        isAnim = false;
      }
    } catch (e) {}
  }

  @override
  void dispose() {
    cGender.dispose();
    givenName.dispose();
    familyName.dispose();
    placeBirth.dispose();
    authority.dispose();
    personalNo.dispose();
    identityCardNo.dispose();
    height.dispose();
    personName.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //parseBackSide([],
    //"NUMER DOWOOU OSOBISTEGO/\nDENTITY CARD NUMER\nNUMER PESEL / PERSONAL NUMBER\n83020410655\nARG 691869\nMIEISCE LRODZENIAI RACE OF BRTH\nWARSZAWA\nNAZWSKO RODOWE IFAMILY NAME\nKOWALSKI\nIMONARODcowIAENTS GIVEN NAMES\nSTEFAN KRYSTYNA\nORGAN WYDAJACY uNG AUTHORITY\nPREZYDENT MIASTA GORZOWA WIELKOPOLSKIEGO\nDATA WYDANIA\nDATE OF ISSUE\n19.05.2029\nI\u003cPOLARG6918690\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\n8202040M2905198POL\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c2\nKOWALSKI\u003cBRZECZYSZCZYKIEWICZ\u003c\u003c\n");
    parseBackSide(
        "Augums / Height/Taille\nDzimums / Sex/ Sexe\n185\nV/M\nIzdošanas datums/ Date of issue / Date de délivrance\n06.03.2012.\nIzdevejiestade / Authority/Autorité\nPMLP RĪGAS 1.NODAĻA\nPersonvärda vesturiska forma /\nThe historical form of the person's name\nANDRIS JĀNIS PARAUDZIŅŠ\nI\u003cLVAPA99929216121282\u003c88882\u003c\u003c\u003c\n8212122M1703054LVA\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c0\nPARAUDZINS\u003c\u003cANDRIS\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\u003c\nSPECIMEN\n");
    final title = "EU ID Card - Back";
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: title,
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    final DateTime dateNow = DateTime.now();
    return SingleChildScrollView(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        //(image != null) ? image : SizedBox(),
        SizedBox(height: 10),
        ScanAnim(file: widget.file, isAnim: isAnim, assetsImage: "eu1b.png"),
        SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InputBox(
                ctrl: identityCardNo,
                lableTxt: "Identity Card No",
                kbType: TextInputType.name,
                len: 50,
                isPwd: false,
              ),
              InputBox(
                ctrl: personalNo,
                lableTxt: "Personal No",
                kbType: TextInputType.name,
                len: 50,
                isPwd: false,
              ),
              InputBox(
                ctrl: personName,
                lableTxt: "Person Name",
                kbType: TextInputType.name,
                len: 100,
                isPwd: false,
              ),
              InputBox(
                ctrl: givenName,
                lableTxt: "Given Name",
                kbType: TextInputType.name,
                len: 50,
                isPwd: false,
              ),
              InputBox(
                ctrl: familyName,
                lableTxt: "Family Name",
                kbType: TextInputType.name,
                len: 50,
                isPwd: false,
              ),
              InputBox(
                ctrl: placeBirth,
                lableTxt: "Place of Birth",
                kbType: TextInputType.name,
                len: 50,
                isPwd: false,
              ),
              InputBox(
                ctrl: authority,
                lableTxt: "Authority",
                kbType: TextInputType.name,
                len: 50,
                isPwd: false,
              ),
              InputBox(
                ctrl: height,
                lableTxt: "Height",
                kbType: TextInputType.number,
                len: 3,
                isPwd: false,
              ),
              SizedBox(height: 20),
              DatePickerView(
                cap: 'Date of issue',
                dt: dtIssue,
                initialDate: DateTime.now(),
                firstDate:
                    DateTime(dateNow.year - 50, dateNow.month, dateNow.day),
                lastDate: DateTime.now(),
                txtColor: Colors.black,
                callback: (value) {
                  if (mounted) {
                    setState(() {
                      try {
                        dtIssue =
                            DateFormat('dd-MMM-yyyy').format(value).toString();
                      } catch (e) {}
                    });
                  }
                },
              ),
              SizedBox(height: 10),
              drawGender(),
              SizedBox(height: 50),
            ],
          ),
        ),
      ]),
    );
  }

  @override
  parseBackSide(String gbody) {
    try {
      //var gbody2 = gbody.replaceAll(".\\n", ".");
      //final gbodyArr = gbody2.split("\\n");

      var gbody2 = gbody.replaceAll(".\n", ".");
      final gbodyArr = gbody2.split("\n");

      for (int i = 0; i < gbodyArr.length; i++) {
        final txt = gbodyArr[i];
        final v = txt.replaceAll("\n", "").toLowerCase();
        print(v);

        //  height
        if (v.startsWith("augums ") ||
            v.contains("height") ||
            v.endsWith("taille")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "").toLowerCase();
          if (v2.startsWith("dzimums	") || v2.contains("sex")) {
            //  height
            String txt2 = gbodyArr[i + 2];
            String v2 = txt2.replaceAll("\n", "").toLowerCase();
            try {
              if (int.parse(v2) > 0) {
                height.text = v2;
              }
            } catch (e) {}
            //  sex
            //
            txt2 = gbodyArr[i + 3];
            v2 = txt2.replaceAll("\n", "").toLowerCase();
            if (v2.length < 4) {
              if (v2.contains('m')) cGender.gender = genderEnum.male.obs;
              if (v2.contains('f')) cGender.gender = genderEnum.female.obs;
            }
          } else {
            try {
              if (int.parse(v2) > 0) {
                height.text = v2;
              }
            } catch (e) {}
          }
        }
        //  sex
        if (v.startsWith("dzimums	") || v.contains("sex")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "").toLowerCase();
          if (v2.length < 4) {
            if (v2.contains('m')) cGender.gender = genderEnum.male.obs;
            if (v2.contains('f')) cGender.gender = genderEnum.female.obs;
          }
        }
        //  date of issue 1st try
        if (v.startsWith("data wydania")) {
          final txt2 = gbodyArr[i + 2];
          final v2 = txt2.replaceAll("\n", "");
          final strArr = v2.split(".");
          if (strArr.length == 3) {
            int pos = int.parse(strArr[1]);
            String mm = Define.listMMM[pos - 1];
            dtIssue = strArr[0] + "-" + mm + "-" + strArr[2];
          }
        }
        //  date of issue 2nd try
        if (dtIssue == '' && v.startsWith("izdošanas	") ||
            (v.contains("date") && v.contains("issue"))) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          final strArr = v2.split(".");
          if (strArr.length > 2) {
            int pos = int.parse(strArr[1]);
            String mm = Define.listMMM[pos - 1];
            dtIssue = strArr[0] + "-" + mm + "-" + strArr[2];
          }
        }
        //  authority
        if ((v.startsWith("izdevėjiestade") || v.startsWith("organ")) ||
            v.contains("authority")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          authority.text = v2;
        }
        //  historical person name
        if (v.contains("historic") && v.contains("person")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          personName.text = v2;
        }
        //  Personal number
        if (v.startsWith("numer pe") || v.contains("personal num")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          personalNo.text = v2;
        }
        //  identity card number try 1
        if (v.startsWith("numer dowoou")) {
          final txt2 = gbodyArr[i + 4];
          final v2 = txt2.replaceAll("\n", "");
          identityCardNo.text = v2;
        }
        //  identity card number try 1
        if (identityCardNo.text.isEmpty && v.startsWith("identity card")) {
          final txt2 = gbodyArr[i + 3];
          final v2 = txt2.replaceAll("\n", "");
          identityCardNo.text = v2;
        }
        //  place of birth
        if (v.startsWith("mieisce") ||
            (v.contains("place") && v.contains("birth"))) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          placeBirth.text = v2;
        }
        //  family name
        if (v.startsWith("nazwsko") || v.contains("family n")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          familyName.text = v2;
        }
        //  given name
        if (v.startsWith("imona") || v.contains("given n")) {
          String txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "").toLowerCase();
          if (v2.startsWith("data wydan")) {
            txt2 = gbodyArr[i + 2];
            v2 = txt2.replaceAll("\n", "");
          }
          if (v2.startsWith("date ") && v2.contains("issue")) {
            txt2 = gbodyArr[i + 3];
            v2 = txt2.replaceAll("\n", "");
          }
          givenName.text = v2;
        }

        /*String v2 = genericParser(
          i,
          gbodyArr,
          v,
          [
            {
              'str': 'height',
              'startWith': false,
              'contains': true,
              'endWith': false
            },
          ],
          [
            {
              'str': 'sex',
              'startWith': false,
              'contains': true,
              'endWith': false
            },
          ],
          null,
        );
        print(v2);*/
      }
    } catch (e) {}
  }

  @override
  parseFrontSide(String gbody) {}
}
