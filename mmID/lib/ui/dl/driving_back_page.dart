import 'dart:convert';
import 'dart:io';
import 'package:aitl/ui/utils/mixin.dart';
import 'package:aitl/ui/anim/ScanAnim.dart';
import 'package:aitl/ui/widgets/InputBox.dart';
import 'package:aitl/ui/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'dart:typed_data';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import '../widgets/DatePickerView.dart';
import '../base_card.dart';
import 'dl_mixin.dart';
import 'package:google_ml_kit/google_ml_kit.dart';

class DrivingBackPage extends StatefulWidget {
  final File file;
  final int index;
  final int width, height;
  const DrivingBackPage(
      {Key key, this.file, @required this.index, this.width, this.height})
      : super(key: key);
  @override
  State createState() => _DrivingBackPageState();
}

class _DrivingBackPageState extends BaseCard<DrivingBackPage>
    with DrivingMixin {
  bool isAnim = true;
  final xLRange = 50;
  final xTRange = 10;

  @override
  void initState() {
    super.initState();
    try {
      if (widget.file != null) {
        Future.delayed(Duration.zero, () async {
          setState(() {
            isAnim = true;
          });

          final textDetector = GoogleMlKit.vision.textDetector();
          final RecognisedText recognisedText =
              await textDetector.processImage(InputImage.fromFile(widget.file));
          print(recognisedText);
          //String text = recognisedText.text;

          String AM10 = "";
          String AM11 = "";
          String AM12 = "";

          for (TextBlock block in recognisedText.blocks) {
            final Rect rect = block.rect;
            final List<Offset> cornerPoints = block.cornerPoints;
            final String text = block.text;
            print(text);

            /*final strArr = text.split(".");
            if (strArr.length > 2) {
              final strArr = text.split(" ");
              if (strArr.length > 1) {
                if (AM10 == '') {
                  try {
                    AM10 = strArr[0];
                    AM11 = strArr[1];
                    AM12 = strArr[2];
                  } catch (e) {}
                }
              }
            }*/
            final List<String> languages = block.recognizedLanguages;
            for (TextLine line in block.lines) {
              // Same getters as TextBlock
              for (TextElement element in line.elements) {
                // Same getters as TextBlock
                print(element.text);
                print(element.rect.toString());
                print(element.cornerPoints.toString());

                final top = element.rect.top;
                final left = element.rect.left;

                //  AM10
                var l = widget.width * .43;
                var t = widget.height * .11;
                if (top >= t && top <= (t + xTRange)) {
                  if (left >= l && left <= (l + xLRange)) {
                    AM10 = element.text;
                  }
                }
              }
            }
          }

          print(AM10);
          print(widget.width.toString());
          print(widget.height.toString());

          //  get by google vision
          /*doGoogleVisionOCR(widget.file).then((body) {
            isAnim = false;
            if (body != null) {
              print(body);
              parseFrontSide(body);
              setState(() {
                isAnim = false;
              });
            }
          });*/
          print("********************************************");
        });
      } else {
        isAnim = false;
      }
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  //  *********************************** parsing start

  parseFrontSide(String gbody) {}

  parseBackSide(String gbody) {}

  //  *********************************** parsing end

  @override
  Widget build(BuildContext context) {
    final title = "Driver's Licence - Back";
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: .8,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          title: Txt(
              txt: title,
              txtColor: Colors.black,
              txtSize: 20,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //(image != null) ? image : SizedBox(),
          SizedBox(height: 10),
          ScanAnim(file: widget.file, isAnim: isAnim, assetsImage: "dl1b.jpg"),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 50),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
