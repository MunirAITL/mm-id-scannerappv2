import 'package:flutter/material.dart';

import 'Txt.dart';

class IcoTxtIco extends StatelessWidget {
  final IconData leftIcon;
  final IconData rightIcon;
  final txt;
  TextAlign txtAlign;
  double leftIconSize;
  double rightIconSize;
  double height;
  Color iconColor;
  Color txtColor;

  IcoTxtIco(
      {Key key,
      @required this.txt,
      @required this.leftIcon,
      @required this.rightIcon,
      this.iconColor = Colors.black,
      this.txtColor = Colors.black,
      this.txtAlign = TextAlign.center,
      this.leftIconSize = 25,
      this.rightIconSize = 30,
      this.height = 10})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //height: getHP(context, 7),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border(
            left: BorderSide(color: Colors.grey, width: 1),
            right: BorderSide(color: Colors.grey, width: 1),
            top: BorderSide(color: Colors.grey, width: 1),
            bottom: BorderSide(color: Colors.grey, width: 1)),
      ),
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 0),
        child: ListTile(
          dense: true,
          contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
          leading: (leftIcon != null)
              ? Icon(
                  leftIcon,
                  color: iconColor,
                  size: leftIconSize,
                )
              : SizedBox(),
          minLeadingWidth: 0,
          title: Txt(
            txt: txt,
            txtColor: txtColor,
            txtSize: 20,
            txtAlign: TextAlign.start,
            isBold: false,
          ),
          trailing: (rightIcon != null)
              ? Icon(
                  rightIcon,
                  color: iconColor,
                  size: rightIconSize,
                )
              : SizedBox(),
        ),
      ),
    );
  }
}
