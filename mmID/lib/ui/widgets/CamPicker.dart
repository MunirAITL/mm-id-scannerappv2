import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import '../utils/mixin.dart';
import 'Txt.dart';

class CamPicker with Mixin {
  List<String> _items = ['From Gallery', 'From Camera', 'Skip'];

  Future<File> _openCamera({isRear = true, String key}) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      preferredCameraDevice: (isRear) ? CameraDevice.rear : CameraDevice.front,
    );
    if (pickedFile != null) {
      File path = File(pickedFile.path);
      return path;
    }
  }

  Future<File> _openGallery({String key}) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    if (pickedFile != null) {
      File path = File(pickedFile.path);
      return path;
    }
  }

  void showCamModal(
      {BuildContext context,
      String title,
      Function callback,
      bool isRear = true}) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Card(
          elevation: 10,
          child: Container(
            color: Colors.white,
            padding: EdgeInsets.all(8),
            height: h(context, 30),
            alignment: Alignment.center,
            child: ListView.separated(
                itemCount: _items.length,
                separatorBuilder: (context, int) {
                  return Divider(
                    color: Colors.grey,
                  );
                },
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: Column(
                      children: [
                        (index == 0)
                            ? Padding(
                                padding: const EdgeInsets.all(20),
                                child: Txt(
                                    txt: title,
                                    txtColor: Colors.black,
                                    txtSize: 22,
                                    txtAlign: TextAlign.center,
                                    isBold: true),
                              )
                            : SizedBox(),
                        Card(
                          child: Txt(
                              txt: _items[index],
                              txtColor: Colors.black,
                              txtSize: 20,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ),
                        (index == 2) ? SizedBox(height: 20) : SizedBox()
                      ],
                    ),
                    onTap: () async {
                      Navigator.pop(context);
                      switch (index) {
                        case 0:
                          callback(await _openGallery()); //  gallery
                          break;
                        case 1:
                          callback(await _openCamera(
                            isRear: isRear,
                          )); //  cam
                          break;
                        default:
                          break;
                      }
                    },
                  );
                }),
          ),
        );
      },
    );
  }
}
