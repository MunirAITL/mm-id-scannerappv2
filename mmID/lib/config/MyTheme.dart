import 'package:flutter/material.dart';

import '../ui/utils/mixin.dart';

class MyTheme {
  static final bool isBoxDeco = false;
  static final double appbarTitleFontSize = 2.3;
  static final double appbarElevation = 3;
  static final double txtSize = 20;
  static final double logoWidth = 40;
  static final double txtLineSpace = 1.3;
  static final String fontFamily = "Arial";
  static Color cyanColor = HexColor.fromHex("#BCEFE6");
  static Color brownColor = HexColor.fromHex('#320B42');
  static final Color greyColor = HexColor.fromHex("#F1F1F4");
  static final Color dgreyColor = HexColor.fromHex("#dac5c5");
  static Color loginBGColor = HexColor.fromHex('#E1E1E5');
  static Color lPurpleColor = HexColor.fromHex("#f0c0c0");
  static Color appbarColor = HexColor.fromHex("#F1F1F1").withAlpha(200);
  static final Color appbarProgColor = Colors.orangeAccent;

  //  timeline
  static final Color timelineTitleColor = HexColor.fromHex("#1F3548");
  static final Color timelinePostCallerNameColor = HexColor.fromHex("#2D2D2D");
  // mycases
  static final Color mycasesNFBtnColor = HexColor.fromHex("#B4B4B4");

  //  theme data
  static final ThemeData refreshIndicatorTheme =
      ThemeData(canvasColor: brownColor, accentColor: Colors.white);

  static final radioThemeData = ThemeData(
    unselectedWidgetColor: Colors.black,
    disabledColor: Colors.black,
    selectedRowColor: Colors.black,
    indicatorColor: Colors.black,
    toggleableActiveColor: Colors.black,
  );

  static final ThemeData themeData = ThemeData(
    // Define the default brightness and colors.
    brightness: Brightness.dark,
    primaryColor: Colors.black,
    accentColor: Colors.black,

    //iconTheme: IconThemeData(color: Colors.white),
    //primarySwatch: Colors.grey,
    //primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
    visualDensity: VisualDensity.adaptivePlatformDensity,
    //scaffoldBackgroundColor: Colors.pink,

    cardTheme: CardTheme(
      color: Colors.white,
    ),

    //primarySwatch: Colors.white,
    primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
    iconTheme: IconThemeData(color: Colors.white),

    /*inputDecorationTheme: InputDecorationTheme(
      focusedBorder:
          UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
    ),*/

    // Define the default font family.
    fontFamily: fontFamily,

    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    /*textTheme: TextTheme(
      headline1: TextStyle(
          fontSize: 72.0, fontWeight: FontWeight.bold, color: Colors.black),
      headline6: TextStyle(
          fontSize: 36.0, fontStyle: FontStyle.italic, color: Colors.black),
      bodyText2: TextStyle(
          fontSize: 14.0, fontFamily: fontFamily, color: Colors.black),
    ),

    buttonTheme: ButtonThemeData(
      buttonColor: Colors.blueAccent,
      shape: RoundedRectangleBorder(),
      textTheme: ButtonTextTheme.accent,
      splashColor: Colors.lime,
    ),*/
  );

  //static final BoxDecoration boxDeco = BoxDecoration(
  //color: Colors.white,
  //);

  static final boxDeco = BoxDecoration(
      border: Border(
          left: BorderSide(color: Colors.grey, width: 1),
          right: BorderSide(color: Colors.grey, width: 1),
          top: BorderSide(color: Colors.grey, width: 1),
          bottom: BorderSide(color: Colors.grey, width: 1)),
      color: Colors.transparent);

  static final picEmboseCircleDeco =
      BoxDecoration(color: Colors.grey, shape: BoxShape.circle, boxShadow: [
    BoxShadow(
      color: Colors.grey.shade500,
      blurRadius: 5.0,
      spreadRadius: 2.0,
    ),
  ]);
}
